# Interface and interaction design Project Design A

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Dockerize

```
docker build -t iid-design-a .
docker run -it -p 8080:80 --rm --name iid-design-a-1 iid-design-a
```

### Podman

```
podman build -t iid-design-a .
podman run -it -p 8080:80 --rm --name iid-design-a-1 iid-design-a
```

### How to use

- Open [IID Listener](https://iid.kateonai.eu/design-a/#/listener) on the mobile phone which represents the device to
  search for.
    - Click the button: start listening (trusted browser event)
    - Hide the phone
    - The browser has to be open, and the screen is not allowed to lock!
- Open [IID Design A](https://iid.kateonai.eu/design-a/#/) on the other mobile phone and activate a beeper there
    - find the now beeping mobile phone
    - PS: If you click the symbol again it will stop beeping
- PS: This stuff can only handle one instance at a time currently maybe I will send a id + input to avoid this  